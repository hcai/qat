cmake_minimum_required(VERSION 3.5.0)

project(QatGenericFunctions VERSION ${Qat_VERSION} LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

file( GLOB SOURCES src/*.cpp )
file( GLOB HEADERS QatGenericFunctions/*.h QatGenericFunctions/*.icc )

find_package(Eigen3 REQUIRED)

add_library( QatGenericFunctions SHARED ${HEADERS} ${SOURCES} )
target_link_libraries( QatGenericFunctions PUBLIC Eigen3::Eigen )
target_include_directories( QatGenericFunctions PUBLIC
   $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
   $<INSTALL_INTERFACE:include> )

set_target_properties( QatGenericFunctions PROPERTIES
   VERSION ${PROJECT_VERSION}
   SOVERSION ${PROJECT_VERSION_MAJOR} )

set(QAT_SOURCES ${QAT_SOURCES} ${SOURCES} PARENT_SCOPE)
set(QAT_HEADERS ${QAT_HEADERS} ${HEADERS} PARENT_SCOPE)
file(MAKE_DIRECTORY ${CMAKE_BINARY_DIR}/qat/include/${PROJECT_NAME})
foreach(header ${HEADERS})
    get_filename_component(header_name ${header} NAME)
    configure_file(${header} ${CMAKE_BINARY_DIR}/qat/include/${PROJECT_NAME}/${header_name} COPYONLY)
endforeach()
include_directories(${CMAKE_BINARY_DIR}/qat/include)  
 
install(TARGETS QatGenericFunctions
    EXPORT ${PROJECT_NAME}-export
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
            COMPONENT          Runtime
            NAMELINK_COMPONENT Development   # Requires CMake 3.12
)

install( FILES ${HEADERS}
   DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/QatGenericFunctions
   COMPONENT Development )

configure_file (pkgconfig/QatGenericFunctions.pc.in pkgconfig/QatGenericFunctions.pc ) 

install (FILES ${CMAKE_BINARY_DIR}/QatGenericFunctions/pkgconfig/QatGenericFunctions.pc
   DESTINATION ${CMAKE_INSTALL_LIBDIR}/pkgconfig
   COMPONENT Development )
 
